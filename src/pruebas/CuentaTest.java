package pruebas;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import logica.Cuenta;

public class CuentaTest {
	private Cuenta cuenta;
	
	@BeforeEach
	public void setUp() {
		this.cuenta = new Cuenta(10, 10000, "Ahorros");
	}
	
	@Test
	public void consignarTest() {			
		this.cuenta.consignar(5000);
		assertEquals(15000, this.cuenta.getSaldo());
	}
	
	@Test
	public void retirarTest() throws Exception {
		this.cuenta.retirar(2000);
		assertEquals(8000, this.cuenta.getSaldo());
		assertThrows(Exception.class, () -> this.cuenta.retirar(9000));
	}
	
	
}
