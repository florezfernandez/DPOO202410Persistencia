package logica;

import java.io.Serializable;

public class Cuenta implements Serializable {
	private int numero;
	private int saldo;
	private String tipo;
	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getSaldo() {
		return saldo;
	}
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Cuenta(int numero, int saldo, String tipo) {
		this.numero = numero;
		this.saldo = saldo;
		this.tipo = tipo;
	}
	
	public Cuenta(int numero) {
		this.numero = numero;
		this.saldo = 0;
		this.tipo = "Ahorros";
	}
	
	public void consignar(int valor) {
		this.saldo += valor;		
	}
	
	public void retirar(int valor) throws Exception  {
		if(valor > this.saldo) {
			throw new Exception("Saldo insuficiente");
		}else {
			this.saldo -= valor;
		}
	}
	
	public void transferir(Cuenta cuentaDestino, int valor) throws Exception {
		this.retirar(valor);
		cuentaDestino.consignar(valor);
	}
	
	@Override
	public String toString() {		
		return this.numero + "\t" + this.saldo + "\t" + this.tipo;
	}
	
}
