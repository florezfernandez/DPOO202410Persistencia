package logica;

import java.util.ArrayList;

import persistencia.ArchivoSerializable;
import persistencia.ArchivoTextoPlano;

public class Banco {
	private ArrayList<Cuenta> cuentas;

	public ArrayList<Cuenta> getCuentas() {
		return cuentas;
	}

	public void setCuentas(ArrayList<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}
	
	public Banco() {
		this.cuentas = new ArrayList<Cuenta>();
	}
	
	public void crearCuenta(int numero, int saldo, String tipo) {
		Cuenta nuevaCuenta = new Cuenta(numero, saldo, tipo);
		this.cuentas.add(nuevaCuenta);
	}
	
	public void almacenarTP() {
		ArrayList<String> textos = new ArrayList<String>();
		for(Cuenta cuenta : this.cuentas) {
			textos.add(cuenta.getNumero() + "," + cuenta.getSaldo() + "," + cuenta.getTipo() + "\n");
		}
		ArchivoTextoPlano.almacenar("cuentas.csv", textos);
	}
	
	public void cargarTP() {
		ArrayList<String> textos = ArchivoTextoPlano.cargar("cuentas.csv");
		for(String texto : textos) {
			String []datos = texto.split(",");
			this.crearCuenta(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), datos[2]);
		}
	}
	
	public void almacenarS() {
		ArchivoSerializable.almacenar("cuentas.ser", this.cuentas);
	}
	
	public void cargarS() {
		this.cuentas = ArchivoSerializable.cargar("cuentas.ser");
	}

}
