package persistencia;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import logica.Cuenta;

public class ArchivoSerializable {

	public static void almacenar(String archivo, ArrayList<Cuenta> objetos) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(archivo));
			oos.writeObject(objetos);
			oos.close();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Cuenta> cargar(String archivo){
		ArrayList<Cuenta> objetos = new ArrayList<Cuenta>();
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(archivo));
			objetos = (ArrayList<Cuenta>)ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objetos;
	}
}
