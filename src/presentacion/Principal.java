package presentacion;

import java.util.Scanner;

import logica.Banco;
import logica.Cuenta;

public class Principal {

	public static void main(String[] args) {
		Banco banco = new Banco();
		Scanner sc = new Scanner(System.in);
		int op = 0;
		do {
			System.out.println("Digite una opcion");
			System.out.println("0. Salir");
			System.out.println("1. Crear Cuenta");
			System.out.println("2. Guardar en archivo de texto plano");
			System.out.println("3. Cargar desde archivo de texto plano");
			System.out.println("4. Imprimir lista de cuentas");
			System.out.println("5. Guardar en archivo serializable");
			System.out.println("6. Cargar desde archivo serializable");
			op = sc.nextInt();
			if(op == 1) {
				System.out.println("Digite numero, saldo y tipo");
				int numero = sc.nextInt();				
				int saldo = sc.nextInt();
				String tipo = sc.next();
				banco.crearCuenta(numero, saldo, tipo);
			}else if(op == 2) {
				banco.almacenarTP();
			}else if(op == 3) {
				banco.cargarTP();
			}else if(op == 4) {
				System.out.println("#\tSaldo\tTipo");
				for(Cuenta cuenta : banco.getCuentas()) {
					System.out.println(cuenta);
				}
			}else if(op == 5){
				banco.almacenarS();
			}else if(op == 6){
				banco.cargarS();
			}
		}while(op != 0);
	}
}
